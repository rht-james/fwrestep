################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/ANTInterface.c \
../Source/HMS_IO.c \
../Source/HMS_Init.c \
../Source/Interruptsd.c \
../Source/LSM330D.c \
../Source/System.c \
../Source/bc_tx.c \
../Source/bs_tx.c \
../Source/ioboard.c \
../Source/main.c \
../Source/main_bsc_tx.c \
../Source/serial.c \
../Source/timer.c 

C_DEPS += \
./Source/ANTInterface.d \
./Source/HMS_IO.d \
./Source/HMS_Init.d \
./Source/Interruptsd.d \
./Source/LSM330D.d \
./Source/System.d \
./Source/bc_tx.d \
./Source/bs_tx.d \
./Source/ioboard.d \
./Source/main.d \
./Source/main_bsc_tx.d \
./Source/serial.d \
./Source/timer.d 

OBJS += \
./Source/ANTInterface.obj \
./Source/HMS_IO.obj \
./Source/HMS_Init.obj \
./Source/Interruptsd.obj \
./Source/LSM330D.obj \
./Source/System.obj \
./Source/bc_tx.obj \
./Source/bs_tx.obj \
./Source/ioboard.obj \
./Source/main.obj \
./Source/main_bsc_tx.obj \
./Source/serial.obj \
./Source/timer.obj 

OBJS__QUOTED += \
"Source\ANTInterface.obj" \
"Source\HMS_IO.obj" \
"Source\HMS_Init.obj" \
"Source\Interruptsd.obj" \
"Source\LSM330D.obj" \
"Source\System.obj" \
"Source\bc_tx.obj" \
"Source\bs_tx.obj" \
"Source\ioboard.obj" \
"Source\main.obj" \
"Source\main_bsc_tx.obj" \
"Source\serial.obj" \
"Source\timer.obj" 

C_DEPS__QUOTED += \
"Source\ANTInterface.d" \
"Source\HMS_IO.d" \
"Source\HMS_Init.d" \
"Source\Interruptsd.d" \
"Source\LSM330D.d" \
"Source\System.d" \
"Source\bc_tx.d" \
"Source\bs_tx.d" \
"Source\ioboard.d" \
"Source\main.d" \
"Source\main_bsc_tx.d" \
"Source\serial.d" \
"Source\timer.d" 

C_SRCS__QUOTED += \
"../Source/ANTInterface.c" \
"../Source/HMS_IO.c" \
"../Source/HMS_Init.c" \
"../Source/Interruptsd.c" \
"../Source/LSM330D.c" \
"../Source/System.c" \
"../Source/bc_tx.c" \
"../Source/bs_tx.c" \
"../Source/ioboard.c" \
"../Source/main.c" \
"../Source/main_bsc_tx.c" \
"../Source/serial.c" \
"../Source/timer.c" 


