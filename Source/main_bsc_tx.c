/*
This software is subject to the license described in the License.txt file
included with this software distribution. You may not use this file except in compliance
with this license.

Copyright (c) Dynastream Innovations Inc. 2013
All rights reserved.
*/

#include "driverlib.h"
#include <stdio.h>
#include "types.h"
#include "antinterface.h"
#include "antdefines.h"
#include "timer.h"
//#include "printf.h"

#include "ioboard.h"
#include "bs_tx.h"
#include "bc_tx.h"
#include "main_bsc_tx.h"
#include "HMS_IO.h"



#define DEBUG_Main_bsc 1
//#undef DEBUG_Main_bsc

#ifdef DEBUG_Main_bsc
unsigned int loopcounter1;
unsigned int antcounter1;
#endif

extern volatile USHORT usLowPowerMode;                          // low power mode control

#define BSTX_ANT_CHANNEL                     ((UCHAR) 0)        // Channels on a single ANT module must be different
#define BCTX_ANT_CHANNEL                     ((UCHAR) 1)        // Channels on a single ANT module must be different


// other defines
#define BSC_PRECISION                             ((ULONG)1000)


static const UCHAR aucNetworkKey[] = ANTPLUS_NETWORK_KEY;

static void ProcessANTBSTXEvents(ANTPLUS_EVENT_RETURN* pstEvent_);
static void ProcessANTBCTXEvents(ANTPLUS_EVENT_RETURN* pstEvent_);
static void ProcessAntEvents(UCHAR* pucEventBuffer_);
unsigned char BSCTX_Counter;
unsigned char buttonvalues;
unsigned char get_running_average;
unsigned char get_running_average2;

unsigned char running_average_cntr;

extern unsigned char FES_on;
unsigned char Previous_FES_on;
unsigned int longpresstime;



//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
// main
//
// main function  
//
// Configures device simulator and BSC TX channel.
//
// \return: This function does not return. 
////////////////////////////////////////////////////////////////////////////
void main_bsbctx(void)
{
   UCHAR* pucRxBuffer;
   ANTPLUS_EVENT_RETURN stBSCEventStruct;
   unsigned char buttoncounter;

   // Main loop
   // Set network key. Do not send other commands
   // until after a response is recieved to this one.
   ANT_NetworkKey(ANTPLUS_NETWORK_NUMBER, aucNetworkKey);

#ifdef DEBUG_Main_bsc
    loopcounter1 = 0;
    antcounter1 = 0;
#endif

    get_running_average = RUN_AV_STATE_0;
    get_running_average2 = RUN_AV_STATE_0;

    /*Initialize FES values*/
    FES_on = FALSE;
    Previous_FES_on = FALSE;


   // Main loop
   while(TRUE)
   {
      /*0-255 counter just gets incremented*/
      buttoncounter++;
      usLowPowerMode = 0;//LPM1_bits;                       // nitialize the low power mode -  move to processor specific file and generic function
      
      pucRxBuffer = ANTInterface_Transaction();                // Check if any data has been recieved from serial
      
#ifdef DEBUG_Main_bsc
      loopcounter1++;
#endif

      if(pucRxBuffer)
      {
#ifdef DEBUG_Main_bsc
          antcounter1++;
#endif
         if(BSTX_ChannelEvent(pucRxBuffer, &stBSCEventStruct))
            usLowPowerMode = 0;

         ProcessANTBSTXEvents(&stBSCEventStruct);

         if(BCTX_ChannelEvent(pucRxBuffer, &stBSCEventStruct))
            usLowPowerMode = 0;

         ProcessANTBCTXEvents(&stBSCEventStruct);
         
         ProcessAntEvents(pucRxBuffer);

      }

      /*if the counter is zero check the buttons*/
      if(buttoncounter == 0)
      {
          /*check the buttons*/
          IOBoard_Check_Button();

          /*Get the states*/
          buttonvalues = IOBoard_Button_Pressed();

          if(buttonvalues&0x01)
          {
              /*Go to state 1 - which is waiting for a button release*/
              get_running_average = RUN_AV_STATE_1;

              IOBoard_Button_Clear(BUTTON1_STATE_OFFSET);
          }

          if((get_running_average == RUN_AV_STATE_1)&&(get_running_average2 != RUN_AV_STATE_1))
          {
             longpresstime = 0;
             get_running_average2 = RUN_AV_STATE_1;
          }
          else if(get_running_average == RUN_AV_STATE_1)
          {
              longpresstime++;

              if(longpresstime > 0x800)
              {
                  Turn_Power_Supply_On_or_Off(OFF);

                  GPIO_setOutputHighOnPin(GPIO_PORT_P3,GPIO_PIN7);
                  GPIO_setOutputHighOnPin(GPIO_PORT_P3,GPIO_PIN6);


                  Timer_DelayTime(1000);
            }

          }


          /*To create a delay simply advance states again*/
          if(get_running_average == RUN_AV_STATE_2)
          {
              get_running_average = RUN_AV_STATE_3;
              get_running_average2 = RUN_AV_STATE_0;
          }

          /*We go to state 2 once the button is released*/
          if((get_running_average == RUN_AV_STATE_1)&&(buttonvalues == 0x0))
          {
              get_running_average = RUN_AV_STATE_2;

         }


          if(get_running_average == RUN_AV_STATE_5)
          {
              get_running_average = RUN_AV_STATE_0;
          }
      }

      if(Previous_FES_on != FES_on)
      {
          Previous_FES_on = FES_on;
          if(FES_on == TRUE)
          {
              /*Set FES trigger pin High*/
              GPIO_setOutputHighOnPin(GPIO_PORT_P6,GPIO_PIN6);
              printf("FES ON\n");
              GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN7);

          }
          else
          {
              /*Set FES trigger pin low*/
              GPIO_setOutputLowOnPin(GPIO_PORT_P6,GPIO_PIN6);
              printf("FES OFF\n");
              GPIO_setOutputHighOnPin(GPIO_PORT_P3,GPIO_PIN7);

          }
      }


      _BIS_SR(usLowPowerMode);                              // Go to sleep if we can  -  move to processor specific file and generic function
   } 
   

} 


////////////////////////////////////////////////////////////////////////////
// ProcessANTBSRXEvents
//
// BSC Reciever event processor  
//
// Processes events recieved from BSC module.
//
// \return: N/A 
///////////////////////////////////////////////////////////////////////////
void ProcessANTBSTXEvents(ANTPLUS_EVENT_RETURN* pstEvent_)
{

   switch(pstEvent_->eEvent)
   {

      case ANTPLUS_EVENT_TRANSMIT:
      {
         IOBOARD_LED3_OUT &= ~IOBOARD_LED3_BIT;                // Turn ON LED
//         printf("BS Tx\n");
         IOBOARD_LED3_OUT |= IOBOARD_LED3_BIT;                 // Turn OFF LED
         break;
      } 
      case ANTPLUS_EVENT_NONE:
      default:
      {
         break;
      }
   }
}


////////////////////////////////////////////////////////////////////////////
// ProcessANTBCRXEvents
//
// BSC Reciever event processor  
//
// Processes events recieved from BSC module.
//
// \return: N/A 
///////////////////////////////////////////////////////////////////////////
void ProcessANTBCTXEvents(ANTPLUS_EVENT_RETURN* pstEvent_)
{

   switch(pstEvent_->eEvent)
   {

      case ANTPLUS_EVENT_TRANSMIT:
      {
         IOBOARD_LED3_OUT &= ~IOBOARD_LED3_BIT;                // Turn ON LED
//         printf("BC Tx %d\n",BSCTX_Counter);
//         printf("%d\n",BSCTX_Counter);
         IOBOARD_LED3_OUT |= IOBOARD_LED3_BIT;                 // Turn OFF LED
         break;
      } 
      case ANTPLUS_EVENT_NONE:
      default:
      {
         break;
      }
   }
}


////////////////////////////////////////////////////////////////////////////
// ProcessAntEvents
//
// Generic ANT Event handling  
//
// \return: N/A 
///////////////////////////////////////////////////////////////////////////
void ProcessAntEvents(UCHAR* pucEventBuffer_)
{
   
   if(pucEventBuffer_)
   {
      UCHAR ucANTEvent = pucEventBuffer_[BUFFER_INDEX_MESG_ID];   
      
      switch( ucANTEvent )
      {
         case MESG_RESPONSE_EVENT_ID:
         {
            switch(pucEventBuffer_[BUFFER_INDEX_RESPONSE_CODE])
            {
               case EVENT_RX_SEARCH_TIMEOUT:
               {
                  IOBOARD_LED0_OUT |= IOBOARD_LED0_BIT;         
                  break;
               }
               case EVENT_TX:
               {
                  break;
               }
               
               case RESPONSE_NO_ERROR:
               {   
                  if (pucEventBuffer_[3] == MESG_OPEN_CHANNEL_ID)
                  {
                     UCHAR ucChannel = pucEventBuffer_[BUFFER_INDEX_CHANNEL_NUM] & 0x1F;
                     
                     // Once the BS channel is opened, open the BC channel
                     if(ucChannel == BSTX_ANT_CHANNEL)
                     {
                        IOBOARD_LED0_OUT &= ~IOBOARD_LED0_BIT; 
                        printf("BS Open.\n");
                        BCTX_Open(BCTX_ANT_CHANNEL,49,5);                                                // proceed with next channel init after response has been received
                     }
                     else
                     {
                        IOBOARD_LED3_OUT &= ~IOBOARD_LED3_BIT; 
                        printf("BC Open.\n");
                     }
                  }
                  else if (pucEventBuffer_[3] == MESG_CLOSE_CHANNEL_ID)
                  {
                  
                  }
                  else if (pucEventBuffer_[3] == MESG_NETWORK_KEY_ID)
                  {
                     //Once we get a response to the set network key
                     //command, start opening the BS channel
                     BSTX_Open(BSTX_ANT_CHANNEL, 49, 5);
                  }
                  break;
               }
            }
         }
      }
   }      
}








