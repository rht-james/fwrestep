/*
 * LSM330D.h
 *
 *  Created on: Mar 14, 2019
 *      Author: RHT-James
 */

#ifndef SOURCE_LSM330D_H_
#define SOURCE_LSM330D_H_

typedef struct
{
    int xAccel;
    int yAccel;
    int zAccel;
} Accel_Data;

typedef struct
{
    int xGyro;
    int yGyro;
    int zGyro;
} Gyro_Data;



/*Accelerometer Initialization Values*/
#define CTRL_R1_INIT    0x67    /*Measure at 200Hz*/
#define CTRL_R2_INIT    0x00
#define CTRL_R3_INIT    0x10
#define CTRL_R4_INIT    0x08
#define CTRL_R5_INIT    0x40
#define CTRL_R6_INIT    0x00

#define REFERENCE_A_INIT    0x00


#define FIFO_CTRL_INIT    0x00

#define INT1_CFG_INIT    0x00
#define INT1_SOURCE_INIT    0x00
#define INT1_THS_INIT    0x00
#define INT1_DURATION_INIT    0x00

#define INT2_CFG_INIT    0x00
#define INT2_SOURCE_INIT    0x00
#define INT2_THS_INIT    0x00
#define INT2_DURATION_INIT    0x00


// LSM330D Register Map
// Accelerometer registers
#define CTRL_REG1_A     0x20
#define CTRL_REG2_A     0x21
#define CTRL_REG3_A     0x22
#define CTRL_REG4_A     0x23
#define CTRL_REG5_A     0x24

#define OUT_X_L_A       0x28
#define OUT_X_H_A       0x29
#define OUT_Y_L_A       0x2A
#define OUT_Y_H_A       0x2B
#define OUT_Z_L_A       0x2C
#define OUT_Z_H_A       0x2D

#define CTRL_REG6_A     0x25
#define REFERENCE_A     0x26
#define STATUS_REG_A    0x27

#define FIFO_CTRL_REG   0x2E
#define FIFO_SRC_REG    0x2F
#define INT1_CFG_A      0x30
#define INT1_SOURCE_A   0x31
#define INT1_THS_A      0x32
#define INT1_DURATION_A 0x33
#define INT2_CFG_A      0x34
#define INT2_SOURCE_A   0x35
#define INT2_THS_A      0x36
#define INT2_DURATION_A 0x37
#define CLICK_CFG_A     0x38
#define CLICK_SRC_A     0x39
#define CLICK_THS_A     0x3A
#define TIME_LIMIT_A    0x3B


#define TIME_LATENCY_A  0x3C
#define TIME_WINDOW_A   0x3D
#define Act_THS         0x3E
#define Act_DUR         0x3F


/*DR - 01, BW 11, PD 1, Zen - 1, Yen - 0, Xen - 0*/
#define CTRL_R1_G_INIT    0x7C  /*ODR at 200Hz - Cutoff - 70 Hz*/
#define CTRL_R2_G_INIT    0x00  /*No filtering*/
#define CTRL_R3_G_INIT    0x00
#define CTRL_R4_G_INIT    0x10  /*500 DegPerSec, */
#define CTRL_R5_G_INIT    0x40

#define REFERENCE_G_INIT    0x00


#define FIFO_CTRL_G_INIT    0x00
#define FIFO_SRC_REG_G_INIT  0x00

#define INT1_CFG_G_INIT    0x00
#define INT1_SRC_G_INIT    0x00
#define INT1_THS_XH_G_INIT    0x00
#define INT1_THS_XL_G_INIT    0x00
#define INT1_THS_YH_G_INIT    0x00
#define INT1_THS_YL_G_INIT    0x00
#define INT1_THS_ZH_G_INIT    0x00
#define INT1_THS_ZL_G_INIT    0x00

#define INT1_DURATION_G_INIT    0x00




// Gyroscope registers
#define WHO_AM_I_G      0x0F
#define WHOAMI_G_VAL    0xD4

#define CTRL_REG1_G     0x20
#define CTRL_REG2_G     0x21
#define CTRL_REG3_G     0x22
#define CTRL_REG4_G     0x23
#define CTRL_REG5_G     0x24

#define REFERENCE_G     0x25
#define OUT_TEMP_G      0x26
#define STATUS_REG_G    0x27

#define OUT_X_L_G       0x28
#define OUT_X_H_G       0x29
#define OUT_Y_L_G       0x2A
#define OUT_Y_H_G       0x2B
#define OUT_Z_L_G       0x2C
#define OUT_Z_H_G       0x2D

#define FIFO_CTRL_REG_G 0x2E
#define INT1_CFG_G      0x30
#define INT1_SRC_G      0x31
#define INT1_THS_XH_G   0x32
#define INT1_THS_XL_G   0x33
#define INT1_THS_YH_G   0x34
#define INT1_THS_YL_G   0x35
#define INT1_THS_ZH_G   0x36
#define INT1_THS_ZL_G   0x37
#define INT1_DURATION_G 0x38

void Gyro_SPI_ChipSelect_Enable(void);
void Gyro_SPI_ChipSelect_Disable(void);
unsigned char Gyro_Read_Reg(unsigned char , BOOL);
void Gyro_Write_Reg(unsigned char, unsigned char );
void Gyro_Setup(void);
void Gyro_Data_Read(Gyro_Data*);



void Accel_SPI_ChipSelect_Enable(void);
void Accel_SPI_ChipSelect_Disable(void);

unsigned char Accel_Read_Reg(unsigned char , BOOL);
void Accel_Write_Reg(unsigned char, unsigned char );
void Accel_Data_Read(Accel_Data*);

unsigned char Accel_Status_Read(void);

#endif /* SOURCE_LSM330D_H_ */
