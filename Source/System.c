/*
   Copyright 2012 Dynastream Innovations, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include "Config.h"
#include "driverlib.h"
#include "System.h"

//------------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////////
/// System_Init
///
/// This function is used initialize the clocks and watchdog parameters
///
/////////////////////////////////////////////////////////////////////////////
void System_Init(void)
{
    /*Set the core voltage higher to run faster*/
    PMM_setVCore(PMM_CORE_LEVEL_3);

    //JMR setting pins not sure if setExternClockSource below does so
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P7,GPIO_PIN2);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P7,GPIO_PIN3);

    /*Initialize the clock sources*/
    UCS_setExternalClockSource(0,XT2_FREQ);

    /*Turn on the External Clock xtal*/
    UCS_turnOnXT2WithTimeout(UCS_XT2_DRIVE_16MHZ_24MHZ,10);

    /*Setup all the clock sources*/
    UCS_initClockSignal(UCS_MCLK,UCS_XT2CLK_SELECT,UCS_CLOCK_DIVIDER_4);
    UCS_initClockSignal(UCS_SMCLK,UCS_XT2CLK_SELECT,UCS_CLOCK_DIVIDER_4);
    UCS_initClockSignal(UCS_ACLK,UCS_XT2CLK_SELECT,UCS_CLOCK_DIVIDER_1);
}

//------------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////////////
/// System_DisableWatchdog
///
/// This function disables the watchdog
///
/////////////////////////////////////////////////////////////////////////////
void System_DisableWatchdog(void)
{
   SYSTEM_WATCHDOG_TCTL = SYSTEM_WATCHDOG_PW | SYSTEM_WATCHDOG_HOLD;
}
