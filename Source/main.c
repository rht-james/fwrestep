#include <stdio.h>
#include <msp430.h>
#include "driverlib.h"
#include "HMS_IO.h"
#include "HMS_Init.h"
#include "timer.h"
#include "ANTInterface.h"
#include "main_bsc_tx.h"
#include "LSM330D.h"


// Look like a global used for timer ...low power mode control
volatile unsigned short usLowPowerMode;

#define Debug_Main

#ifdef Debug_Main
unsigned long debug_value;
#endif


int main(void) {
    unsigned int i,k;
    unsigned char accel_check;

    /*Initialize the Pin to turn on the power supply - Time sensitive*/
    Initialize_Power_Supply_CTRL_PIN();
    Turn_Power_Supply_On_or_Off(ON);

    WDT_A_hold(WDT_A_BASE);

    printf("Hello World!\n");

    GPIO_setAsOutputPin(GPIO_PORT_P3,GPIO_PIN6);
    GPIO_setAsOutputPin(GPIO_PORT_P3,GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
    GPIO_setOutputHighOnPin(GPIO_PORT_P3,GPIO_PIN7);

    /*Setup pin for external trigger*/
    GPIO_setAsOutputPin(GPIO_PORT_P6,GPIO_PIN6);
    GPIO_setOutputLowOnPin(GPIO_PORT_P6,GPIO_PIN6);



    /*Initializations to run*/
    Initializations();

    /* Setup the timer*/
    Timer_Init();

#ifdef Debug_Main
    debug_value = UCS_getSMCLK();
#endif

    Accel_SPI_ChipSelect_Disable();

    for(i=0; i<2; i++)
    {
        /*Turn the LED ON*/
        GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);

        /*Delay 0.1 seconds*/
        for(k=0; k<10; k++)
        {
            Timer_DelayTime(1000);
        }

        /*Turn the LED OFF*/
        GPIO_setOutputHighOnPin(GPIO_PORT_P3,GPIO_PIN6);

        /*Delay 0.9 seconds*/
        for(k=0; k<40; k++)
        {
            Timer_DelayTime(1000);
        }

    }

    /*Read the Gyro Who_am_I*/
    accel_check =  Gyro_Read_Reg(WHO_AM_I_G,0);

    printf("Who_AM_I = %X \n",accel_check);

    /*Reset Memory Contents*/
    Accel_Write_Reg(CTRL_REG5_A,0xC0);

    /*Get the REG1 byte*/
    accel_check = Accel_Read_Reg(CTRL_REG5_A,0);

    printf("REG5 = %X \n",accel_check);


    /*Get the REG1 byte*/
    accel_check = Accel_Read_Reg(CTRL_REG1_A,0);
    printf("REG1 = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_R1_INIT)
    {
        Accel_Write_Reg(CTRL_REG1_A,CTRL_R1_INIT);
    }


    /*Get the REG2 byte*/
    accel_check = Accel_Read_Reg(CTRL_REG2_A,0);
    printf("REG2 = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_R2_INIT)
    {
        Accel_Write_Reg(CTRL_REG2_A,CTRL_R2_INIT);
    }


    /*Get the REG3 byte*/
    accel_check = Accel_Read_Reg(CTRL_REG3_A,0);
    printf("REG3 = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_R3_INIT)
    {
        Accel_Write_Reg(CTRL_REG3_A,CTRL_R2_INIT);
    }


    /*Get the REG4 byte*/
    accel_check = Accel_Read_Reg(CTRL_REG4_A,0);
    printf("REG4 = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_R4_INIT)
    {
        Accel_Write_Reg(CTRL_REG4_A,CTRL_R4_INIT);
    }


    /*Get the FIFO_CTRL_REG byte*/
    accel_check = Accel_Read_Reg(FIFO_CTRL_REG,0);
    printf("FIFO_CTRL = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != FIFO_CTRL_INIT)
    {
        Accel_Write_Reg(FIFO_CTRL_REG,FIFO_CTRL_INIT);
    }


    /*Get the CTRL_REG6_A byte*/
    accel_check = Accel_Read_Reg(CTRL_REG6_A,0);
    printf("CTRL_REG6_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_R6_INIT)
    {
        Accel_Write_Reg(CTRL_REG6_A,CTRL_R6_INIT);
    }


    /*Get the CTRL_REG6_A byte*/
    accel_check = Accel_Read_Reg(REFERENCE_A,0);
    printf("REFERENCE_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != REFERENCE_A_INIT)
    {
        Accel_Write_Reg(REFERENCE_A,REFERENCE_A_INIT);
    }

    /*Get the INT1_CFG_A byte*/
    accel_check = Accel_Read_Reg(INT1_CFG_INIT,0);
    printf("INT1_CFG_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_CFG_INIT)
    {
        Accel_Write_Reg(INT1_CFG_A,INT1_CFG_INIT);
    }


    /*Get the INT1_SOURCE_A byte*/
    accel_check = Accel_Read_Reg(INT1_SOURCE_A,0);
    printf("INT1_SOURCE_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_SOURCE_INIT)
    {
        Accel_Write_Reg(INT1_SOURCE_A,INT1_SOURCE_INIT);
    }


    /*Get the INT1_THS_A byte*/
    accel_check = Accel_Read_Reg(INT1_THS_A,0);
    printf("INT1_THS_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_THS_INIT)
    {
        Accel_Write_Reg(INT1_THS_A,INT1_THS_INIT);
    }

    /*Get the INT1_DURATION_A byte*/
    accel_check = Accel_Read_Reg(INT1_DURATION_A,0);
    printf("INT1_DURATION_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_DURATION_INIT)
    {
        Accel_Write_Reg(INT1_DURATION_A,INT1_DURATION_INIT);
    }


    /*Get the INT2_SOURCE_A byte*/
    accel_check = Accel_Read_Reg(INT2_SOURCE_A,0);
    printf("INT2_SOURCE_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT2_SOURCE_INIT)
    {
        Accel_Write_Reg(INT2_SOURCE_A,INT2_SOURCE_INIT);
    }


    /*Get the INT2_THS_A byte*/
    accel_check = Accel_Read_Reg(INT2_THS_A,0);
    printf("INT2_THS_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT2_THS_INIT)
    {
        Accel_Write_Reg(INT2_THS_A,INT2_THS_INIT);
    }


    /*Get the INT2_DURATION_A byte*/
    accel_check = Accel_Read_Reg(INT2_DURATION_A,0);
    printf("INT2_DURATION_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT2_DURATION_INIT)
    {
        Accel_Write_Reg(INT2_DURATION_A,INT2_DURATION_INIT);
    }


    /*Get the INT2_CFG_A byte*/
    accel_check = Accel_Read_Reg(INT2_CFG_A,0);
    printf("INT2_CFG_A = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != 0x0)
    {
        Accel_Write_Reg(INT2_CFG_A,0x0);
    }


    /*Turn off the boot flag Memory Contents*/
    Accel_Write_Reg(CTRL_REG5_A,CTRL_R5_INIT);

    /*Function sets up the Gyro*/
    Gyro_Setup();


    __enable_interrupt();                        // Enable global interrupts

    ANTInterface_Init();                         // Initialize ANT to serial interface

    main_bsbctx();                               // Run bike speed and bike cadence TX main thread - does not return.


    return (0);
}



