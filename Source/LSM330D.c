/*
 * LSM330D.c
 *
 *  Created on: Mar 14, 2019
 *      Author: RHT-James
 *      Code for the LSM330D Accel/Gyro IC
 */

#include "driverlib.h"
#include "types.h"
#include "LSM330D.h"
#include <stdio.h>

char SPI_byte_read[7];
unsigned char Accel_Status;
unsigned char Accel_FIFO_SRC;


/********************************************************************************************
 * Gyro_SPI_ChipSelect_Enable()
 * This functions will Activate the CS for the Accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Gyro_SPI_ChipSelect_Enable(void)
{
    /*CS is active Low so Set low to Enable*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P1,GPIO_PIN7);
}

/********************************************************************************************
 * Gyro_SPI_ChipSelect_Disable()
 * This functions will Activate the CS for the Accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Gyro_SPI_ChipSelect_Disable(void)
{
    /*CS is active Low so Set low to Enable*/
    GPIO_setOutputHighOnPin(GPIO_PORT_P1,GPIO_PIN7);
}

/********************************************************************************************
 * Gyro_SPI_ChipSelect_Disable()
 * This functions will Activate the CS for the Accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
unsigned char  Gyro_Read_Reg(unsigned char address, BOOL increment)
{
    unsigned char SPI_byte_out;
    unsigned char SPI_byte_read;

    /*Set the Chipselect*/
    Gyro_SPI_ChipSelect_Enable();

    __delay_cycles(40);

    SPI_byte_out = address;

    /*For reads set the Most significant bit high*/
    SPI_byte_out |= 0x80;

    /*If the increment is non zero increment addresses after every read*/
    if(increment != 0) SPI_byte_out |= 0x40;

    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_byte_out);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Send out a zero (read mode) to clock in the register value to read*/
    SPI_byte_out =  0x80;

    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_byte_out);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    SPI_byte_read = USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Set the Chipselect*/
    Gyro_SPI_ChipSelect_Disable();

    __delay_cycles(40);


    return(SPI_byte_read);

}


/********************************************************************************************
 * Accel_SPI_ChipSelect_Enable()
 * This functions will Activate the CS for the Accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Accel_SPI_ChipSelect_Enable(void)
{
    /*CS is active Low so Set low to Enable*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P1,GPIO_PIN4);
}

/********************************************************************************************
 * Accel_SPI_ChipSelect_Disable()
 * This functions will Activate the CS for the Accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Accel_SPI_ChipSelect_Disable(void)
{
    /*CS is active Low so Set low to Enable*/
    GPIO_setOutputHighOnPin(GPIO_PORT_P1,GPIO_PIN4);
}

/********************************************************************************************
 * Accel_Read_Reg()
 * This functions will read a single register from the accelerometer.
 *
 *
*********************************************************************************************/
unsigned char Accel_Read_Reg(unsigned char address, BOOL increment)
{
    unsigned char SPI_byte_out;
    unsigned char SPI_byte_read;

    /*Set the Chipselect*/
    Accel_SPI_ChipSelect_Enable();

    __delay_cycles(40);

    SPI_byte_out = address;

    /*For reads set the Most significant bit high*/
    SPI_byte_out |= 0x80;

    /*If the increment is non zero increment addresses after every read*/
    if(increment != 0) SPI_byte_out |= 0x40;

    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_byte_out);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Send out a zero (read mode) to clock in the register value to read*/
    SPI_byte_out =  0x80;

    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_byte_out);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    SPI_byte_read = USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Set the Chipselect*/
    Accel_SPI_ChipSelect_Disable();

    __delay_cycles(40);


    return(SPI_byte_read);
}


/********************************************************************************************
 * Gyro_Write_Reg()
 * This functions will write a byte value to a register of the Gyro.
 *
 *
*********************************************************************************************/
void Gyro_Write_Reg(unsigned char address, unsigned char byte_out)
{
    unsigned char SPI_byte_out;

    /*Set the Chipselect*/
    Gyro_SPI_ChipSelect_Enable();

    __delay_cycles(40);

    SPI_byte_out = address;

    /*If the increment is non zero increment addresses after every read*/
 //   if(increment != 0) SPI_byte_out |= 0x40;

    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_byte_out);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Send out a zero (read mode) to clock in the register value to read*/
    SPI_byte_out =  byte_out;

    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_byte_out);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    /*Disable the Chipselect*/
    Gyro_SPI_ChipSelect_Disable();

    __delay_cycles(40);

}


/********************************************************************************************
 * Accel_write_Reg()
 * This functions will write a byte value to a register of the accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Accel_Write_Reg(unsigned char address, unsigned char byte_out)
{
    unsigned char SPI_byte_out;

    /*Set the Chipselect*/
    Accel_SPI_ChipSelect_Enable();

    __delay_cycles(40);

    SPI_byte_out = address;

    /*If the increment is non zero increment addresses after every read*/
 //   if(increment != 0) SPI_byte_out |= 0x40;

    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_byte_out);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Send out a zero (read mode) to clock in the register value to read*/
    SPI_byte_out =  byte_out;

    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_byte_out);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    /*Disable the Chipselect*/
    Accel_SPI_ChipSelect_Disable();

    __delay_cycles(40);

}

/********************************************************************************************
 * Accel_Data_Read()
 * This functions will write a byte value to a register of the accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Accel_Data_Read(Accel_Data* paccel)
{
    unsigned char SPI_Reg_Add;
    unsigned char cnt;

    /*Set the Chipselect*/
    Accel_SPI_ChipSelect_Enable();

    __delay_cycles(20);

//    SPI_Reg_Add = STATUS_REG_A | 0x80;

    /*Send out the first address*/
 //   USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

    /*set to zero*/
//    for(cnt=0; cnt<6; cnt++)
//    {
//        SPI_byte_read[cnt]=0;
//    }

    /*wait for the byte to be sent out*/
    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    /*The address with the Read bit set high in addition to the auto increment*/
    SPI_Reg_Add = OUT_X_L_A | 0xC0;

    /*Send out the first address*/
    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

    /*Increment to read next byte*/
    SPI_Reg_Add = 0x80;

    /*wait for the byte to be sent out*/
    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    SPI_byte_read[6] = USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Read back 6 consecutive values*/
    for(cnt=0; cnt<6; cnt++)
    {
        /*Clock out bytes to read*/
        USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

        /*wait for the byte to be sent out*/
        while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

        SPI_byte_read[cnt] = USCI_A_SPI_receiveData(USCI_A1_BASE);

        /*Increment to read next byte*/
 //       SPI_Reg_Add += 1;

        if((SPI_byte_read[0] == 0)&&(SPI_byte_read[1])==0)
        {
            SPI_byte_read[0] = 0x77;
        }

    }

    paccel->xAccel = (int)SPI_byte_read[1]<<8;
    paccel->xAccel |= SPI_byte_read[0];
    paccel->xAccel = (int)(paccel->xAccel/16);

    paccel->yAccel = (int)SPI_byte_read[3]<<8;
    paccel->yAccel |= SPI_byte_read[2];
    paccel->yAccel = (int)(paccel->yAccel/16);

    paccel->zAccel = (int)SPI_byte_read[5]<<8;
    paccel->zAccel |= SPI_byte_read[4];
    paccel->zAccel = (int)(paccel->zAccel/16);

    /*Disable the Chipselect*/
    Accel_SPI_ChipSelect_Disable();

    __delay_cycles(5);

}


/********************************************************************************************
 * Gyro_Status_Read()
 * This functions will write a byte value to a register of the accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
unsigned char Gyro_Status_Read(void)
{
    unsigned char SPI_Reg_Add;
    unsigned char Status_byte_read;

    /*Set the Chipselect*/
    Accel_SPI_ChipSelect_Enable();

    __delay_cycles(20);

    SPI_Reg_Add = STATUS_REG_A | 0x80;

    /*Send out the first address*/
    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

    /*wait for the byte to be sent out*/
    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    /*Send out the Second byte to read value*/
    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

    /*wait for the byte to be sent out*/
    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    Status_byte_read = USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Set the Chipselect*/
    Accel_SPI_ChipSelect_Disable();

    __delay_cycles(20);

    return(Status_byte_read);
}


/********************************************************************************************
 * Gyro_Data_Read()
 * This functions will write a byte value to a register of the accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Gyro_Data_Read(Gyro_Data* pgyro)
{
    unsigned char SPI_Reg_Add;
    unsigned char cnt;

    /*Set the Chipselect*/
    Gyro_SPI_ChipSelect_Enable();

    __delay_cycles(20);

//*wait for the byte to be sent out*/
    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    /*The address with the Read bit set high in addition to the auto increment*/
    SPI_Reg_Add = OUT_X_L_G | 0xC0;

    /*Send out the first address*/
    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

    /*Increment to read next byte*/
    SPI_Reg_Add = 0x80;

    /*wait for the byte to be sent out*/
    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    SPI_byte_read[6] = USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Read back 6 consecutive values*/
    for(cnt=0; cnt<6; cnt++)
    {
        /*Clock out bytes to read*/
        USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

        /*wait for the byte to be sent out*/
        while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

        SPI_byte_read[cnt] = USCI_A_SPI_receiveData(USCI_A1_BASE);
    }

    pgyro->xGyro = (int)SPI_byte_read[1]<<8;
    pgyro->xGyro |= SPI_byte_read[0];
    pgyro->xGyro = (int)(pgyro->xGyro/16);

    pgyro->yGyro = (int)SPI_byte_read[3]<<8;
    pgyro->yGyro |= SPI_byte_read[2];
    pgyro->yGyro = (int)(pgyro->yGyro/16);

    pgyro->zGyro = (int)SPI_byte_read[5]<<8;
    pgyro->zGyro |= SPI_byte_read[4];
    pgyro->zGyro = (int)(pgyro->zGyro/16);

    /*Disable the Chipselect*/
    Gyro_SPI_ChipSelect_Disable();

    __delay_cycles(5);

}


/********************************************************************************************
 * Accel_Status_Read()
 * This functions will write a byte value to a register of the accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
unsigned char Accel_Status_Read(void)
{
    unsigned char SPI_Reg_Add;
    unsigned char Status_byte_read;

    /*Set the Chipselect*/
    Accel_SPI_ChipSelect_Enable();

    __delay_cycles(20);

    SPI_Reg_Add = STATUS_REG_A | 0x80;

    /*Send out the first address*/
    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

    /*wait for the byte to be sent out*/
    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    /*Send out the Second byte to read value*/
    USCI_A_SPI_transmitData(USCI_A1_BASE,SPI_Reg_Add);

    /*wait for the byte to be sent out*/
    while(USCI_A_SPI_isBusy(USCI_A1_BASE)==USCI_A_SPI_BUSY);

    Status_byte_read = USCI_A_SPI_receiveData(USCI_A1_BASE);

    /*Set the Chipselect*/
    Accel_SPI_ChipSelect_Disable();

    __delay_cycles(20);

    return(Status_byte_read);
}



/********************************************************************************************
 * Gyro_Setup()
 * This functions will write a byte value to a register of the accelerometer.
 *
 * The pin used for this is P1.4 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Gyro_Setup(void)
{
    unsigned char accel_check;

    /*Reset Memory Contents*/
    Gyro_Write_Reg(CTRL_REG5_G,0xC0);

    /*Get the REG1 byte*/
    accel_check = Gyro_Read_Reg(CTRL_REG5_G,0);

    printf("REG5G = %X \n",accel_check);


    /*Get the REG1 byte*/
    accel_check = Gyro_Read_Reg(CTRL_REG1_G,0);
    printf("REG1G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_R1_G_INIT)
    {
        Gyro_Write_Reg(CTRL_REG1_G,CTRL_R1_G_INIT);
    }


    /*Get the REG2 byte*/
    accel_check = Gyro_Read_Reg(CTRL_REG2_G,0);
    printf("REG2G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_REG2_G)
    {
        Gyro_Write_Reg(CTRL_REG2_G,CTRL_R2_G_INIT);
    }


    /*Get the REG3 byte*/
    accel_check = Gyro_Read_Reg(CTRL_REG3_G,0);
    printf("REG3G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_R3_INIT)
    {
        Gyro_Write_Reg(CTRL_REG3_G,CTRL_R2_INIT);
    }


    /*Get the REG4 byte*/
    accel_check = Gyro_Read_Reg(CTRL_REG4_G,0);
    printf("REG4G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != CTRL_R4_G_INIT)
    {
        Gyro_Write_Reg(CTRL_REG4_G,CTRL_R4_G_INIT);
    }


    /*Get the FIFO_CTRL_REG byte*/
    accel_check = Gyro_Read_Reg(FIFO_CTRL_REG_G,0);
    printf("FIFO_CTRLG = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != FIFO_CTRL_G_INIT)
    {
        Gyro_Write_Reg(FIFO_CTRL_REG_G,FIFO_CTRL_G_INIT);
    }


    /*Get the CTRL_REG6_A byte*/
    accel_check = Gyro_Read_Reg(REFERENCE_G,0);
    printf("REFERENCE_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != REFERENCE_G_INIT)
    {
        Gyro_Write_Reg(REFERENCE_G,REFERENCE_G_INIT);
    }


    /*Get the INT1_CFG_A byte*/
    accel_check = Gyro_Read_Reg(INT1_CFG_G,0);
    printf("INT1_CFG_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_CFG_G_INIT)
    {
        Gyro_Write_Reg(INT1_CFG_G,INT1_CFG_G_INIT);
    }


    /*Get the INT1_SOURCE_A byte*/
    accel_check = Gyro_Read_Reg(INT1_SRC_G,0);
    printf("INT1_SOURCE_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_SRC_G_INIT)
    {
        Gyro_Write_Reg(INT1_SRC_G,INT1_SRC_G_INIT);
    }


    /*Get the INT1_THS_A byte*/
    accel_check = Gyro_Read_Reg(INT1_THS_XH_G,0);
    printf("INT1_THS_XH_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_THS_XH_G_INIT)
    {
        Gyro_Write_Reg(INT1_THS_XH_G,INT1_THS_XH_G_INIT);
    }


    /*Get the INT1_DURATION_A byte*/
    accel_check = Gyro_Read_Reg(INT1_THS_XL_G,0);
    printf("INT1_THS_XL_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_THS_XL_G_INIT)
    {
        Gyro_Write_Reg(INT1_THS_XL_G,INT1_THS_XL_G_INIT);
    }


    /*Get the INT2_SOURCE_A byte*/
    accel_check = Gyro_Read_Reg(INT1_THS_YH_G,0);
    printf("INT1_THS_YH_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_THS_YH_G_INIT)
    {
        Gyro_Write_Reg(INT1_THS_YH_G,INT1_THS_YH_G_INIT);
    }


    /*Get the INT2_THS_A byte*/
    accel_check = Gyro_Read_Reg(INT1_THS_YL_G,0);
    printf("INT1_THS_YL_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_THS_YL_G_INIT)
    {
        Gyro_Write_Reg(INT1_THS_YL_G,INT1_THS_YL_G_INIT);
    }


    /*Get the INT2_DURATION_A byte*/
    accel_check = Gyro_Read_Reg(INT1_THS_ZH_G,0);
    printf("INT1_THS_ZH_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_THS_ZH_G_INIT)
    {
        Gyro_Write_Reg(INT1_THS_ZH_G,INT1_THS_ZH_G_INIT);
    }


    /*Get the INT1_THS_ZL_G byte*/
    accel_check = Gyro_Read_Reg(INT1_THS_ZL_G,0);
    printf("INT1_THS_ZL_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_THS_ZL_G_INIT)
    {
        Gyro_Write_Reg(INT1_THS_ZL_G,INT1_THS_ZL_G_INIT);
    }


    /*Get the INT1_THS_ZL_G byte*/
    accel_check = Gyro_Read_Reg(INT1_DURATION_G,0);
    printf("INT1_DURATION_G = %X \n",accel_check);
    /*If the register is not correct write to it*/
    if(accel_check != INT1_DURATION_G_INIT)
    {
        Gyro_Write_Reg(INT1_DURATION_G,INT1_DURATION_G_INIT);
    }


    /*Turn off the boot flag Memory Contents*/
    Gyro_Write_Reg(CTRL_REG5_A,CTRL_R5_INIT);


}
