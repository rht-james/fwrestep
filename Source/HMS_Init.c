/*
 * HMS_Init.c
 *
 *  Created on: Feb 27, 2019
 *      Author: RHT-James
 */
#include "driverlib.h"
#include "HMS_Init.h"
#include <usci_a_spi.h>



/********************************************************************************************
 * Initializations()
 * This functions will initialize the I/O pin for power supply control.
 *
 * The pin used for this is P5.6 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void Initializations(void)
{
    /*Set the core voltage higher to run faster*/
    PMM_setVCore(PMM_CORE_LEVEL_3);

    //JMR setting pins not sure if setExternClockSource below does so
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P7,GPIO_PIN2);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P7,GPIO_PIN3);

    /*Initialize the clock sources*/
    UCS_setExternalClockSource(0,XT2_FREQ);

    /*Turn on the External Clock xtal*/
    UCS_turnOnXT2WithTimeout(UCS_XT2_DRIVE_16MHZ_24MHZ,10);

    /*Setup all the clock sources*/
    UCS_initClockSignal(UCS_MCLK,UCS_XT2CLK_SELECT,UCS_CLOCK_DIVIDER_1);
    UCS_initClockSignal(UCS_SMCLK,UCS_XT2CLK_SELECT,UCS_CLOCK_DIVIDER_4);
    UCS_initClockSignal(UCS_ACLK,UCS_XT2CLK_SELECT,UCS_CLOCK_DIVIDER_1);

    ACCEL_SPI_Init();

}

/********************************************************************************************
 * Initializations()
 * This functions will initialize the I/O pin for power supply control.
 *
 * The pin used for this is P5.6 High Power supply OFF, Low Power supply an Output pin
 * Sets up the clocks for the IC
*********************************************************************************************/
void ACCEL_SPI_Init(void)
{
    USCI_A_SPI_initMasterParam SPI_A_params;
    uint32_t smclk_freq;

    /*Set P3.4 low this is DEN-G (not sure what it does)*/
    GPIO_setAsOutputPin(GPIO_PORT_P3,GPIO_PIN4);
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN4);


    /*obtain the SMCLK frequency value*/
    smclk_freq = UCS_getSMCLK();

    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8,GPIO_PIN1);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8,GPIO_PIN2);
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P8,GPIO_PIN3);

    SPI_A_params.selectClockSource = USCI_A_SPI_CLOCKSOURCE_SMCLK;
    SPI_A_params.clockSourceFrequency = smclk_freq;
    SPI_A_params.desiredSpiClock = Accel_SPI_Freq;
    SPI_A_params.clockPhase = USCI_A_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT;

//    SPI_A_params.clockPhase = USCI_A_SPI_PHASE_DATA_CAPTURED_ONFIRST_CHANGED_ON_NEXT;
//    SPI_A_params.clockPolarity = USCI_A_SPI_CLOCKPOLARITY_INACTIVITY_LOW;
    SPI_A_params.clockPolarity = USCI_A_SPI_CLOCKPOLARITY_INACTIVITY_HIGH;
    SPI_A_params.msbFirst = USCI_A_SPI_MSB_FIRST;
//    SPI_A_params.msbFirst = USCI_A_SPI_LSB_FIRST;

    /*Initialize the SPI module*/
    USCI_A_SPI_initMaster(USCI_A1_BASE,&SPI_A_params);

    /*Setup the CS pins for the Accel/Gyro*/
    /*Set the pins high*/
    GPIO_setOutputHighOnPin(GPIO_PORT_P1,GPIO_PIN4);
    GPIO_setOutputHighOnPin(GPIO_PORT_P1,GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P1,GPIO_PIN4);
    GPIO_setAsOutputPin(GPIO_PORT_P1,GPIO_PIN7);
    GPIO_setOutputHighOnPin(GPIO_PORT_P1,GPIO_PIN4);
    GPIO_setOutputHighOnPin(GPIO_PORT_P1,GPIO_PIN7);


    /*Enable the SPI*/
    USCI_A_SPI_enable(USCI_A1_BASE);

}

