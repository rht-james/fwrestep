/*
 * main_bsc_tx.h
 *
 *  Created on: Mar 5, 2019
 *      Author: RHT-James
 */

#ifndef SENSOR_BSBC_MAIN_BSC_TX_H_
#define SENSOR_BSBC_MAIN_BSC_TX_H_

void main_bsbctx(void);

#endif /* SENSOR_BSBC_MAIN_BSC_TX_H_ */
