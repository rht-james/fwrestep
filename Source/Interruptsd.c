/*
 * Interruptsd.c
 *
 *  Created on: Mar 12, 2019
 *      Author: RHT-James
 */
#include "driverlib.h"


//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt PORT4_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=PORT4_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_PORT4_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

/*
 * Interruptsd.c
 *
 *  Created on: Mar 12, 2019
 *      Author: RHT-James
 */


//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt TIMER2_A1_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=TIMER2_A1_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_TIMER2_A1_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}


//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt TIMER2_A0_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=TIMER2_A0_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_TIMER2_A0_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt DAC12_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=DAC12_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_DAC12_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}



//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt RTC_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=RTC_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_RTC_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt PORT2_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=PORT2_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_PORT2_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt USCI_A1_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=USCI_A1_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_USCI_A1_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt PORT1_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=PORT1_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_PORT1_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}


//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt DMA_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=DMA_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_DMA_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt LDO_PWR_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=LDO_PWR_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_LDO_PWR_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt TIMER0_A1_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=TIMER1_A1_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_TIMER0_A1_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt TIMER0_A0_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=TIMER1_A0_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_TIMER0_A0_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt ADC12_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=ADC12_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_ADC12_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt USCI_B0_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=USCI_B0_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_USCI_B0_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt USCI_A0_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=USCI_A0_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_USCI_A0_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt WDT_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=WDT_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_WDT_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt TIMER0_B1_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=TIMER0_B1_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_TIMER0_B1_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt TIMER0_B0_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=TIMER0_B0_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_TIMER0_B0_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt COMP_B_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=COMP_B_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_COMP_B_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt UNMI_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=UNMI_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_UNMI_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}

//----------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////
///
// Interrupt SYSNMI_VECTOR
//
//
////////////////////////////////////////////////////////////////////////////
#pragma vector=SYSNMI_VECTOR // TCCR0 Interrupt Vector for TIMER A
__interrupt void INT_SYSNMI_VECTOR(void)
{
    /*Turn the LED ON*/
    GPIO_setOutputLowOnPin(GPIO_PORT_P3,GPIO_PIN6);
}






