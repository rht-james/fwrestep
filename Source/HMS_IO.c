/*
 * HMS_IO.c
 *
 *  Created on: Feb 27, 2019
 *      Author: RHT-James
 */
#include "driverlib.h"
#include "HMS_IO.h"

/********************************************************************************************
 * Initialize_Power_Supply_CTRL_PIN()
 * This functions will initialize the I/O pin for power supply control.
 *
 * The pin used for this is P5.6 High Power supply OFF, Low Power supply an Output pin
*********************************************************************************************/
void Initialize_Power_Supply_CTRL_PIN(void)
{
    GPIO_setAsOutputPin(GPIO_PORT_P5,GPIO_PIN6);

}


/********************************************************************************************
 * Turn_Power_Supply_On_or_Off()
 * This functions will turn the power supply ON or OFF depending on the variable passed to it.
 * The function will turn OFF if OFF is passed.  Anything else is ON.
 * The pin used for this is P5.6 High Power supply OFF, Low Power supply On
*********************************************************************************************/
void Turn_Power_Supply_On_or_Off(char ON_or_OFF)
{
    if(ON_or_OFF == OFF)
    {
        GPIO_setOutputLowOnPin(GPIO_PORT_P5,GPIO_PIN6);

    }
    else
    {
        GPIO_setOutputHighOnPin(GPIO_PORT_P5,GPIO_PIN6);
    }
}

