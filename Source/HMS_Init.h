/*
 * HMS_Init.h
 *
 *  Created on: Feb 27, 2019
 *      Author: RHT-James
 */

#ifndef SOURCE_HMS_INIT_H_
#define SOURCE_HMS_INIT_H_

//crystal is 20 MHz
#define XT2_FREQ 20000000

//crystal is 20 MHz
#define Accel_SPI_Freq 1000000



void Initializations(void);

void ACCEL_SPI_Init(void);


#endif /* SOURCE_HMS_INIT_H_ */
