/*
 * HMS_IO.h
 *
 *  Created on: Feb 27, 2019
 *      Author: RHT-James
 */

#ifndef HEADERS_HMS_IO_H_
#define HEADERS_HMS_IO_H_

#define ON 1
#define OFF 0


void Initialize_Power_Supply_CTRL_PIN(void);
void Turn_Power_Supply_On_or_Off(char);



#endif /* HEADERS_HMS_IO_H_ */
